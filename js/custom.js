$(document).ready(function () {
    $('#master-card,#bkash,#rocket').hide();
    $('#medium').change(function () {
        val=$(this).val();
        if(val=='master_card'){
            $("#master-card").show();
            $("#bkash").hide();
            $("#rocket").hide();

        }
        if(val=='bkash'){
            $("#bkash").show();
            $("#rocket").hide();
            $("#master-card").hide();
        }
        if(val=='rocket'){
            $("#rocket").show();
            $("#bkash").hide();
            $("#master-card").hide();
        }
        if(val==''){
            $('#master-card,#bkash,#rocket').hide();
        }

    })
});

//owl carousel
$(document).ready(function(){
    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:30,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            250:{
                items:3,
                nav:false
            },
            750:{
                items:5,
                nav:true,
                loop:false
            },
            1000:{
                items:6,
                nav:true,
                loop:false
            }
        }
    })
});

//slider
$(document).ready(function() {
    $('#media').carousel({
        pause: true,
        interval: false,
    });
});